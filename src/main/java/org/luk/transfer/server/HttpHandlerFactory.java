package org.luk.transfer.server;

import io.undertow.server.HttpHandler;

public interface HttpHandlerFactory {
    HttpHandler getTransferHttpHandler();

    HttpHandler getAccountHttpHandler();

    HttpHandler get404HttpHandler();
}
