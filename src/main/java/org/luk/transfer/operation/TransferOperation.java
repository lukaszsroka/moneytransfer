package org.luk.transfer.operation;

import java.math.BigDecimal;

public interface TransferOperation {

    void transfer(BigDecimal amount, int fromAccountId, int toAccountId);

}
