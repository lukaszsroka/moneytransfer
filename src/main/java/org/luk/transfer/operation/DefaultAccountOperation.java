package org.luk.transfer.operation;

import org.luk.transfer.account.Account;
import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.factory.AccountFactory;

import java.math.BigDecimal;
import java.util.Optional;

public class DefaultAccountOperation implements AccountOperation {

    private AccountRepository accountRepository;

    public DefaultAccountOperation(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void addAccount(int id, BigDecimal amount) {
        accountRepository.save(AccountFactory.createAccount(id, amount));
    }

    @Override
    public Optional<Account> getAccount(int id) {
        return accountRepository.findById(id);
    }
}
