package org.luk.transfer.factory;

import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.operation.AccountOperation;
import org.luk.transfer.operation.TransferOperation;
import org.luk.transfer.server.HttpHandlerFactory;

public interface MoneyTransferFactory {

    HttpHandlerFactory getHttpHandlerFactory();

    AccountOperation getAccountOperation(AccountRepository accountRepository);

    TransferOperation getTransferOperation(AccountRepository accountRepository);

    AccountRepository getAccountRepository();
}
