package org.luk.transfer.factory;

import org.luk.transfer.account.Account;
import org.luk.transfer.account.DefaultAccount;

import java.math.BigDecimal;

public class AccountFactory {

    public static Account createAccount(int id, BigDecimal amount) {
        return new DefaultAccount(id, amount);
    }
}
