package org.luk.transfer.exceptions;

public class NoAccountException extends RuntimeException {

    public NoAccountException(String message) {
        super(message);
    }
}
