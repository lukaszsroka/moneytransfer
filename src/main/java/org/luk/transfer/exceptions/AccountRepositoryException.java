package org.luk.transfer.exceptions;

public class AccountRepositoryException extends RuntimeException {

    public AccountRepositoryException(String message) {
        super(message);
    }
}
