package org.luk.transfer.exceptions;

public class NotEnoughMoneyTransferException extends RuntimeException {

    public NotEnoughMoneyTransferException(String message) {
        super(message);
    }
}
