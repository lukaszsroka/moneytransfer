package org.luk.transfer.account;

import org.luk.transfer.exceptions.AccountRepositoryException;
import org.luk.transfer.exceptions.UniqueAccountRepositoryEception;
import org.luk.transfer.factory.AccountFactory;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;

public class InMemoryAccountRepositoryTest {

    @Test
    public void addAndFindTest() {
//        Given
        InMemoryAccountRepository repo = new InMemoryAccountRepository();
        int id = 1;
        Account account = AccountFactory.createAccount(id, BigDecimal.valueOf(12));

//        When
        repo.save(account);
        Optional<Account> resultAccount = repo.findById(id);

//        Then
        assertTrue(resultAccount.isPresent());
    }

    @Test(expected = UniqueAccountRepositoryEception.class)
    public void duplicateIdTest() {
//        Given
        InMemoryAccountRepository repo = new InMemoryAccountRepository();
        Account first = AccountFactory.createAccount(1, BigDecimal.valueOf(12));
        Account second = AccountFactory.createAccount(1, BigDecimal.valueOf(14));

//        When
        repo.save(first);
        repo.save(second);
    }

    @Test(expected = AccountRepositoryException.class)
    public void nullTest() {
//        Given
        InMemoryAccountRepository repo = new InMemoryAccountRepository();

//        When
        repo.save(null);
    }

    @Test
    public void findNotExisting() {
//        Given
        InMemoryAccountRepository repo = new InMemoryAccountRepository();

//        When
        Optional<Account> result = repo.findById(1);

//        Then
        assertFalse(result.isPresent());
    }

}