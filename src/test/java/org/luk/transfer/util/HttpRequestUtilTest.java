package org.luk.transfer.util;

import org.luk.transfer.exceptions.RequestParameterException;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;

import static org.junit.Assert.*;

public class HttpRequestUtilTest {

    @Test
    public void getIdParamTest() {
        getIdTypeParamTest(HttpRequestUtil.ID_PARAM, HttpRequestUtil::getIdParam, Integer::new);
    }

    @Test(expected = RequestParameterException.class)
    public void getIdParamNullTest() {
        getIdTypeParamNullTest(HttpRequestUtil::getIdParam);
    }

    @Test(expected = RequestParameterException.class)
    public void getIdParamNotNumberTest() {
        getIdTypeParamNotNumberTest(HttpRequestUtil::getIdParam, HttpRequestUtil.ID_PARAM);
    }

    @Test
    public void getFromAccountParamTest() {
        getIdTypeParamTest(HttpRequestUtil.FROM_ID_PARAM, HttpRequestUtil::getFromAccountParam, Integer::new);
    }

    @Test(expected = RequestParameterException.class)
    public void getFromAccountParamNullTest() {
        getIdTypeParamNullTest(HttpRequestUtil::getFromAccountParam);
    }

    @Test(expected = RequestParameterException.class)
    public void getFromAccountParamNotNumberTest() {
        getIdTypeParamNotNumberTest(HttpRequestUtil::getFromAccountParam, HttpRequestUtil.FROM_ID_PARAM);
    }

    @Test
    public void getToAccountParamTest() {
        getIdTypeParamTest(HttpRequestUtil.TO_ID_PARAM, HttpRequestUtil::getToAccountParam, Integer::new);
    }

    @Test(expected = RequestParameterException.class)
    public void getToAccountParamNullTest() {
        getIdTypeParamNullTest(HttpRequestUtil::getToAccountParam);
    }

    @Test(expected = RequestParameterException.class)
    public void getToAccountParamNotNumberTest() {
        getIdTypeParamNotNumberTest(HttpRequestUtil::getToAccountParam, HttpRequestUtil.TO_ID_PARAM);
    }

    @Test
    public void getAmountParam() {
        getIdTypeParamTest(HttpRequestUtil.AMOUNT_PARAM, HttpRequestUtil::getAmountParam, BigDecimal::valueOf);
    }

    @Test(expected = RequestParameterException.class)
    public void getAmountParamNullTest() {
        getIdTypeParamNullTest(HttpRequestUtil::getAmountParam);
    }

    @Test(expected = RequestParameterException.class)
    public void getAmountParamNotNumberTest() {
        getIdTypeParamNotNumberTest(HttpRequestUtil::getAmountParam, HttpRequestUtil.AMOUNT_PARAM);
    }


    private <T> void getIdTypeParamTest(String paramName, Function<Map<String, Deque<String>>, T> functionToTest, Function<Integer, T> supplier) {
//        Given
        Map<String, Deque<String>> params = new HashMap<>();
        params.put(paramName, new LinkedList<>(Collections.singletonList("1")));

//        When
        T result = functionToTest.apply(params);

//        Then
        assertEquals(supplier.apply(1), result);
    }

    private <T> void getIdTypeParamNullTest(Function<Map<String, Deque<String>>, T> function) {
//        Given
        Map<String, Deque<String>> params = new HashMap<>();

//        When
        function.apply(params);
    }


    private <T> void getIdTypeParamNotNumberTest(Function<Map<String, Deque<String>>, T> function, String paramName) {
//        Given
        Map<String, Deque<String>> params = new HashMap<>();
        params.put(paramName, new LinkedList<>(Collections.singletonList("a")));

//        When
        function.apply(params);
    }
}